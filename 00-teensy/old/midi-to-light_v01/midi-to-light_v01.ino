#include <Adafruit_NeoPixel.h>
#include <avr/power.h>

#define STRIP_PIN        7
#define MATRIX_PIN       8

#define STRIP_PIXELS     47
#define MATRIX_PIXELS    16 //42

Adafruit_NeoPixel strip = Adafruit_NeoPixel(STRIP_PIXELS, STRIP_PIN, NEO_RGB + NEO_KHZ800);
Adafruit_NeoPixel matrix = Adafruit_NeoPixel(MATRIX_PIXELS, MATRIX_PIN, NEO_RGB + NEO_KHZ800);

int colorRed = 0;
int colorBlue = 0;
int colorGreen = 0;

int matrixRed = 0;
int matrixBlue = 0;
int matrixGreen = 0;

int lastCC = 0;

byte noteReceived = 0;
byte noteOffReceived = 0;
bool off = false;
bool cc = false;

void setup() {
 Serial.begin(9600);
  strip.begin();
  matrix.begin();

  usbMIDI.setHandleNoteOff(onNoteOff);
  usbMIDI.setHandleNoteOn(onNoteOn);
  usbMIDI.setHandleControlChange(onControlChange);

 for(int i=0;i<STRIP_PIXELS;i++) {
      strip.setPixelColor(i, strip.Color(255, 0, 0));
      matrix.setPixelColor(i, matrix.Color(255, 0, 0));

      delay(5);
      strip.show(); 
      matrix.show();
  } 
  delay(500);

   stripsOn(55,55,55);
   delay(250);
   clearStrips();
}

void loop() {
  usbMIDI.read();
}

void onNoteOn(byte channel, byte note, byte velocity) {
  //**MIDI CC//
  if (note == 53) {
  panOutStrip(colorRed, colorGreen, colorBlue, lastCC);
  panOutMatrix(colorRed, colorGreen, colorBlue, lastCC);
           
  //**attribuer ici la note (ON) midi pour allumer tout le strip**
  //(note == nb de la note en midi)**
  //**setup Labotanic note = 36**  
  } else if (note == 54) {
    stripOn(colorRed, colorGreen, colorBlue);

  //**attribuer ici les notes (ON) midi pour allumer segements respectifs
  //**(note == nb de la note en midi)**
  //**setuo Labotanic note de 37 à 40**  
  } else if (note == 55) {
    stripFirst(colorRed, colorGreen, colorBlue);
  } else if (note == 56) {
    stripSecond(colorRed, colorGreen, colorBlue);
  } else if (note == 57) {
    stripThird(colorRed, colorGreen, colorBlue);
  } else if (note == 58) {
    stripFourth(colorRed, colorGreen, colorBlue);
//**attribuer ici les notes (ON) midi pour allumer la matrice
//**(note == nb de la note en midi)**
//**setup Labotanic note = 54**    
  } else if (note == 60) {
    matrixOn(matrixRed, matrixGreen, matrixBlue);
  }
  off = false;
  noteReceived = note;
}

void onNoteOff(byte channel, byte note, byte velocity) {
  // Serial.write("Note off received");
  
  //**MIDI CC//
  if (note == 53) {
    clearstrip();
  }
      
  //**attribuer ici la note (OFF) midi pour eteindre tout le strip**
  //(note == nb de la note en midi)**
  //**setup Labotanic note = 36**  
  else if (note == 54) {
    clearStrips();
  }
    
  //**attribuer ici les notes (OFF) midi pour eteindre la matrice
  //**(note == nb de la note en midi)**
  //**setup Labotanic note = 36**   
  else if (note == 60) {
    clearmatrix();
  }
    
  //**attribuer ici les notes (ON) midi pour eteindre les segements respectifs
  //**(note == nb de la note en midi)**
  //**setup Labotanic note de 37 à 40**   
  else if (note == 55 || 56 || 57 ||58 ) {
    clearstrip();
  } 
  
    off = true;
  noteOffReceived = note;
}

void onControlChange(byte channel, byte controlType, byte value) {
  if(!off) {
    if(controlType == 2) {
      if(noteReceived == 53) {
        panOutStrip(colorRed, colorGreen, colorBlue, (int)value);
        panOutMatrix (colorRed, colorGreen, colorBlue, (int)value);
        lastCC = (int)value;
      }
//**choix des couleurs STRIP -MIDI CC 22/27/28**
      } else if (controlType == 3) {
        if(noteReceived == 54 || noteReceived == 58) {
          colorRed = (int)value*2;
          stripOn(colorRed, colorGreen, colorBlue);
        } else {
            colorRed = (int)value*2;
        }
    }
    } else if (controlType == 4) {
        if(noteReceived == 54 || noteReceived == 58) {
          colorGreen = (int)value*2;
          stripOn(colorRed, colorGreen, colorBlue);
        } else {
            colorGreen = (int)value*2;
        }
    } else if (controlType == 5) {
        if(noteReceived == 54 || noteReceived == 58) {
          colorBlue = (int)value*2;
          stripOn(colorRed, colorGreen, colorBlue);
        } else {
            colorBlue = (int)value*2;
        }
//**choix des couleurs MATRIX -MIDI CC 22/27/28**        
    } else if(controlType == 6) {
        if(noteReceived == 60) {
          matrixRed = (int)value*2;
          matrixOn(matrixRed, matrixGreen, matrixBlue);
        } else {
            matrixRed = (int)value*2;
        }
    } else if (controlType == 7) {
        if(noteReceived == 60) {
          matrixGreen = (int)value*2;
          matrixOn(matrixRed, matrixGreen, matrixBlue);
        } else {
            matrixGreen = (int)value*2;
        }
    } else if (controlType == 8) {
        if(noteReceived == 60) {
          matrixBlue = (int)value*2;
          matrixOn(matrixRed, matrixGreen, matrixBlue);
        } else {
            matrixBlue = (int)value*2;
        }
    }
  }


void stripsOn(int red, int green, int blue) {
  stripOn(red, green, blue);
  matrixOn(red, green, blue);
}

void stripsOff() {
  stripOn(0, 0, 0);
  matrixOn(0, 0, 0);
  strip.show();
  matrix.show();
}

void panOutStrip(int red, int green, int blue, int control) {
  //Go through the loop 'control' amount of times
  //Loop? Needs to increment and decrement at the same time
  //Two variables- the more 'control' the more LEDs light up
  //72 leds in Strip -> always minus 1 when coding
  //middle LEDs are 35+36 with -1
  //can't compare LED position with control- control needs to be relative
 
  // control = (35/127)*control;
  // Serial.write("Coverted control is " + control);
  // Serial.write("Last control is " + lastCC);


  
  if(!off) {  

    int starting = 35 - control;
    int ending = control + 36;

    if (starting < 0) {
      starting = 0;
    }

    if (ending > 46) {
      ending = 46;
    }

    //led less than needs to be 35
    //need to subtrac number off from 35

    for (int i=35;i>starting;i--) {
      strip.setPixelColor(i, strip.Color(red, green, blue));
    }
    for (int i=36;i<ending;i++) {
      strip.setPixelColor(i, strip.Color(red, green, blue));
    }

    strip.show();
  }
}

void panOutMatrix(int red, int green, int blue, int control) {
  //Go through the loop 'control' amount of times
  //Loop? Needs pto increment and decrement at the same time
  //Two variables- the more 'control' the more LEDs light up
  //72 leds in Strip -> always minus 1 when coding
  //middle LEDs are 35+36 with -1
  //can't compare LED position with control- control needs to be relative
 
  // control = (35/127)*control;
  // Serial.write("Coverted control is " + control);
  // Serial.write("Last control is " + lastCC);


  
  if(!off) {  

    int starting = 35 - control;
    int ending = control + 36;

    if (starting < 0) {
      starting = 0;
    }

    if (ending > 15) {
      ending = 15;
    }

    //led less than needs to be 35
    //need to subtrac number off from 35

    for (int i=35;i>starting;i--) {
      matrix.setPixelColor(i, matrix.Color(red, green, blue));
    }
    for (int i=36;i<ending;i++) {
      matrix.setPixelColor(i, matrix.Color(red, green, blue));
    }

    matrix.show();
  }
}

void stripFirst(int red, int green, int blue) {
  for (int i=0;i<12;i++) {
    strip.setPixelColor(i, strip.Color(red, green, blue));
  }

  strip.show();

  colorRed = red;
  colorGreen = green;
  colorBlue = blue;
}

void stripSecond(int red, int green, int blue) {
  for (int i=12;i<24;i++) {
    strip.setPixelColor(i, strip.Color(red, green, blue));
  }

  strip.show();

  colorRed = red;
  colorGreen = green;
  colorBlue = blue;
}

void stripThird(int red, int green, int blue) {
  for (int i=24;i<35;i++) {
    strip.setPixelColor(i, strip.Color(red, green, blue));
  }

  strip.show();

  colorRed = red;
  colorGreen = green;
  colorBlue = blue;
}

void stripFourth(int red, int green, int blue) {
  for (int i=35;i<47;i++) {
    strip.setPixelColor(i, strip.Color(red, green, blue));
  }

  strip.show();

  colorRed = red;
  colorGreen = green;
  colorBlue = blue;
}

void stripOn(int red, int green, int blue) {
  
  for(int i=0;i<STRIP_PIXELS;i++) {
      strip.setPixelColor(i, strip.Color(red, green, blue)); 
  } 

  strip.show();
  
  colorRed = red;
  colorGreen = green;
  colorBlue = blue;
}

void matrixOn(int red, int green, int blue) {  

  for(int i=0;i<MATRIX_PIXELS;i++) {
      matrix.setPixelColor(i, matrix.Color(red, green, blue)); 
  } 
  
  matrix.show();
  
  matrixRed = red;
  matrixGreen = green;
  matrixBlue = blue;
}

void clearStrips() {
  clearstrip();
  clearmatrix();
}

void clearstrip() {
  for(int i=0;i<STRIP_PIXELS;i++){
    strip.setPixelColor(i, strip.Color(0, 0, 0));
  } 
  strip.show();
}

void clearmatrix() {
  for(int i=0;i<MATRIX_PIXELS;i++){
    matrix.setPixelColor(i, matrix.Color(0, 0, 0));
  } 
  matrix.show();
}
