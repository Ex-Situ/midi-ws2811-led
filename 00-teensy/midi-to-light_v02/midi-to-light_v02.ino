#include <Adafruit_NeoPixel.h>
#include <avr/power.h>

#define STRIP1_PIN        7
#define STRIP2_PIN        14
#define MATRIX_PIN        8

#define STRIP1_PIXELS     47
#define STRIP2_PIXELS     42
#define MATRIX_PIXELS     16

Adafruit_NeoPixel strip1 = Adafruit_NeoPixel(STRIP1_PIXELS, STRIP1_PIN, NEO_RGB + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(STRIP2_PIXELS, STRIP2_PIN, NEO_RGB + NEO_KHZ800);
Adafruit_NeoPixel matrix = Adafruit_NeoPixel(MATRIX_PIXELS, MATRIX_PIN, NEO_RGB + NEO_KHZ800);

int strip1Red = 0;
int strip1Blue = 0;
int strip1Green = 0;

int strip2Red = 0;
int strip2Blue = 0;
int strip2Green = 0;

int matrixRed = 0;
int matrixBlue = 0;
int matrixGreen = 0;

int lastCC = 0;

byte noteReceived = 0;
byte noteOffReceived = 0;
bool off = false;
bool cc = false;

void setup() {
 Serial.begin(9600);
  strip1.begin();
  strip2.begin();
  matrix.begin();

  usbMIDI.setHandleNoteOff(onNoteOff);
  usbMIDI.setHandleNoteOn(onNoteOn);
  usbMIDI.setHandleControlChange(onControlChange);
/*
 for(int i=0;i<STRIP1_PIXELS;i++) {
      strip1.setPixelColor(i, strip1.Color(255, 0, 0));
      strip2.setPixelColor(i, strip2.Color(255, 0, 0));
      matrix.setPixelColor(i, matrix.Color(255, 0, 0));

      delay(5);
      strip1.show();
      strip2.show(); 
      matrix.show();
  }
*/
 for(int i=0;i<STRIP1_PIXELS;i++) {
      strip1.setPixelColor(i, strip1.Color(255, 0, 0));
      delay(10);
      strip1.show();
  }   
 for(int i=0;i<STRIP2_PIXELS;i++) {
      strip2.setPixelColor(i, strip2.Color(0, 255, 0));
      delay(10);
      strip2.show();
  }
 for(int i=0;i<MATRIX_PIXELS;i++) {
      matrix.setPixelColor(i, matrix.Color(0, 0, 255));
      delay(10);
      matrix.show();
  }  
  
  delay(100);

   stripsOn(55,55,55);
   delay(1000);
   clearstrip1();
   clearstrip2();
   clearmatrix();
}

void loop() {
  usbMIDI.read();
}

void onNoteOn(byte channel, byte note, byte velocity) {
  //**MIDI CC//
  if (note == 51) {
  panOutStrip1(strip1Red, strip1Green, strip1Blue, lastCC);
  panOutStrip2(strip2Red, strip2Green, strip2Blue, lastCC);
  panOutMatrix(matrixRed, matrixGreen, matrixBlue, lastCC);
           
  //**attribuer ici la note (ON) midi pour allumer tout le strip**
  //(note == nb de la note en midi)**
  //**setup Labotanic note = 36**  
  } else if (note == 54) {
    strip1On(strip1Red, strip1Green, strip1Blue);
  //**attribuer ici les notes (ON) midi pour allumer segements respectifs
  //**(note == nb de la note en midi)**
  //**setuo Labotanic note de 37 à 40**  
  } else if (note == 55) {
    strip1First(strip1Red, strip1Green, strip1Blue);
  } else if (note == 56) {
    strip1Second(strip1Red, strip1Green, strip1Blue);
  } else if (note == 57) {
    strip1Third(strip1Red, strip1Green, strip1Blue);
  } else if (note == 58) {
    strip1Fourth(strip1Red, strip1Green, strip1Blue);

  //**attribuer ici la note (ON) midi pour allumer tout le strip**
  //(note == nb de la note en midi)**
  //**setup Labotanic note = 36**  
  } else if (note == 59) {
    strip2On(strip2Red, strip2Green, strip2Blue);
  //**attribuer ici les notes (ON) midi pour allumer segements respectifs
  //**(note == nb de la note en midi)**
  //**setuo Labotanic note de 37 à 40**  
  } else if (note == 60) {
    strip2First(strip2Red, strip2Green, strip2Blue);
  } else if (note == 61) {
    strip2Second(strip2Red, strip2Green, strip2Blue);
  } else if (note == 62) {
    strip2Third(strip2Red, strip2Green, strip2Blue);
  } else if (note == 63) {
    strip2Fourth(strip2Red, strip2Green, strip2Blue);

//**attribuer ici les notes (ON) midi pour allumer la matrice
//**(note == nb de la note en midi)**
//**setup Labotanic note = 54**    
  } else if (note == 64) {
    matrixOn(matrixRed, matrixGreen, matrixBlue);
  }
  
  off = false;
  noteReceived = note;
}

void onNoteOff(byte channel, byte note, byte velocity) {
  // Serial.write("Note off received");
  
  //**MIDI CC//
  if (note == 51) {
    clearstrip1();
    clearstrip2();
    clearmatrix();
  }
      
  //**attribuer ici la note (OFF) midi pour eteindre tout le strip1**
  //(note == nb de la note en midi)**
  //**setup Labotanic note = 36**  
  else if (note == 54) {
    clearstrip1();
  }
  
  //**attribuer ici la note (OFF) midi pour eteindre tout le strip1**
  //(note == nb de la note en midi)**
  //**setup Labotanic note = 36**  
  else if (note == 59) {
  clearstrip2();
  }  
   
  //**attribuer ici les notes (ON) midi pour eteindre les segements respectifs
  //**(note == nb de la note en midi)**
  //**setup Labotanic note de 37 à 40**   
  if (note == 55 || 56 || 57 ||58 ) {
    clearstrip1();
  }

    //**attribuer ici les notes (ON) midi pour eteindre les segements respectifs
  //**(note == nb de la note en midi)**
  //**setup Labotanic note de 37 à 40**   
  if (note == 60 || 61 || 62 ||63 ) {
    clearstrip2();
  }

  //**attribuer ici les notes (OFF) midi pour eteindre la matrice
  //**(note == nb de la note en midi)**
  //**setup Labotanic note = 36**   
  if (note == 64) {
    clearmatrix();
  }
  
    off = true;
  noteOffReceived = note;
}

void onControlChange(byte channel, byte controlType, byte value) {
  if(!off) {
    if(controlType == 2) {
      if(noteReceived == 51) {
        panOutStrip1(strip1Red, strip1Green, strip1Blue, (int)value);
        panOutStrip1(strip2Red, strip2Green, strip2Blue, (int)value);
        panOutMatrix (matrixRed, matrixGreen, matrixBlue, (int)value);
        lastCC = (int)value;
      }
//**choix des couleurs STRIP -MIDI CC 22/27/28**
      } else if (controlType == 3) {
        if(noteReceived == 54 || noteReceived == 58) {
          strip1Red = (int)value*2;
          strip1On(strip1Red, strip1Green, strip1Blue);
        } else {
            strip1Red = (int)value*2;
        }
    }
    } else if (controlType == 4) {
        if(noteReceived == 54 || noteReceived == 58) {
          strip1Green = (int)value*2;
          strip1On(strip1Red, strip1Green, strip1Blue);
        } else {
            strip1Green = (int)value*2;
        }
    } else if (controlType == 5) {
        if(noteReceived == 54 || noteReceived == 58) {
          strip1Blue = (int)value*2;
          strip1On(strip1Red, strip1Green, strip1Blue);
        } else {
            strip1Blue = (int)value*2;
        }
//**choix des couleurs MATRIX -MIDI CC 22/27/28**        
    } else if(controlType == 6) {
        if(noteReceived == 60) {
          matrixRed = (int)value*2;
          matrixOn(matrixRed, matrixGreen, matrixBlue);
        } else {
            matrixRed = (int)value*2;
        }
    } else if (controlType == 7) {
        if(noteReceived == 60) {
          matrixGreen = (int)value*2;
          matrixOn(matrixRed, matrixGreen, matrixBlue);
        } else {
            matrixGreen = (int)value*2;
        }
    } else if (controlType == 8) {
        if(noteReceived == 60) {
          matrixBlue = (int)value*2;
          matrixOn(matrixRed, matrixGreen, matrixBlue);
        } else {
            matrixBlue = (int)value*2;
        }
    }
  }


void stripsOn(int red, int green, int blue) {
  strip1On(red, green, blue);
  strip2On(red, green, blue);
  matrixOn(red, green, blue);
}

void stripsOff() {
  strip1On(0, 0, 0);
  strip2On(0, 0, 0);
  matrixOn(0, 0, 0);
  strip1.show();
  strip2.show();
  matrix.show();
}

void panOutStrip1(int red, int green, int blue, int control) {
  //Go through the loop 'control' amount of times
  //Loop? Needs to increment and decrement at the same time
  //Two variables- the more 'control' the more LEDs light up
  //72 leds in Strip -> always minus 1 when coding
  //middle LEDs are 35+36 with -1
  //can't compare LED position with control- control needs to be relative
 
  // control = (35/127)*control;
  // Serial.write("Coverted control is " + control);
  // Serial.write("Last control is " + lastCC);


  
  if(!off) {  

    int starting = 35 - control;
    int ending = control + 36;

    if (starting < 0) {
      starting = 0;
    }

    if (ending > 46) {
      ending = 46;
    }

    //led less than needs to be 35
    //need to subtrac number off from 35

    for (int i=35;i>starting;i--) {
      strip1.setPixelColor(i, strip1.Color(red, green, blue));
    }
    for (int i=36;i<ending;i++) {
      strip1.setPixelColor(i, strip1.Color(red, green, blue));
    }

    strip1.show();
  }
}

void panOutStrip2(int red, int green, int blue, int control) {
  //Go through the loop 'control' amount of times
  //Loop? Needs to increment and decrement at the same time
  //Two variables- the more 'control' the more LEDs light up
  //72 leds in Strip -> always minus 1 when coding
  //middle LEDs are 35+36 with -1
  //can't compare LED position with control- control needs to be relative
 
  // control = (35/127)*control;
  // Serial.write("Coverted control is " + control);
  // Serial.write("Last control is " + lastCC);


  
  if(!off) {  

    int starting = 35 - control;
    int ending = control + 36;

    if (starting < 0) {
      starting = 0;
    }

    if (ending > 46) {
      ending = 46;
    }

    //led less than needs to be 35
    //need to subtrac number off from 35

    for (int i=35;i>starting;i--) {
      strip2.setPixelColor(i, strip2.Color(red, green, blue));
    }
    for (int i=36;i<ending;i++) {
      strip2.setPixelColor(i, strip2.Color(red, green, blue));
    }

    strip2.show();
  }
}

void panOutMatrix(int red, int green, int blue, int control) {
  //Go through the loop 'control' amount of times
  //Loop? Needs pto increment and decrement at the same time
  //Two variables- the more 'control' the more LEDs light up
  //72 leds in Strip -> always minus 1 when coding
  //middle LEDs are 35+36 with -1
  //can't compare LED position with control- control needs to be relative
 
  // control = (35/127)*control;
  // Serial.write("Coverted control is " + control);
  // Serial.write("Last control is " + lastCC);


  
  if(!off) {  

    int starting = 35 - control;
    int ending = control + 36;

    if (starting < 0) {
      starting = 0;
    }

    if (ending > 15) {
      ending = 15;
    }

    //led less than needs to be 35
    //need to subtrac number off from 35

    for (int i=35;i>starting;i--) {
      matrix.setPixelColor(i, matrix.Color(red, green, blue));
    }
    for (int i=36;i<ending;i++) {
      matrix.setPixelColor(i, matrix.Color(red, green, blue));
    }

    matrix.show();
  }
}

void strip1First(int red, int green, int blue) {
  for (int i=0;i<12;i++) {
    strip1.setPixelColor(i, strip1.Color(red, green, blue));
  }

  strip1.show();

  strip1Red = red;
  strip1Green = green;
  strip1Blue = blue;
}

void strip1Second(int red, int green, int blue) {
  for (int i=12;i<24;i++) {
    strip1.setPixelColor(i, strip1.Color(red, green, blue));
  }

  strip1.show();

  strip1Red = red;
  strip1Green = green;
  strip1Blue = blue;
}

void strip1Third(int red, int green, int blue) {
  for (int i=24;i<35;i++) {
    strip1.setPixelColor(i, strip1.Color(red, green, blue));
  }

  strip1.show();

  strip1Red = red;
  strip1Green = green;
  strip1Blue = blue;
}

void strip1Fourth(int red, int green, int blue) {
  for (int i=35;i<47;i++) {
    strip1.setPixelColor(i, strip1.Color(red, green, blue));
  }

  strip1.show();

  strip1Red = red;
  strip1Green = green;
  strip1Blue = blue;
}

void strip1On(int red, int green, int blue) {
  
  for(int i=0;i<STRIP1_PIXELS;i++) {
      strip1.setPixelColor(i, strip1.Color(red, green, blue)); 
  } 

  strip1.show();
  
  strip1Red = red;
  strip1Green = green;
  strip1Blue = blue;
}

void strip2First(int red, int green, int blue) {
  for (int i=0;i<11;i++) {
    strip2.setPixelColor(i, strip2.Color(red, green, blue));
  }

  strip2.show();

  strip2Red = red;
  strip2Green = green;
  strip2Blue = blue;
}

void strip2Second(int red, int green, int blue) {
  for (int i=11;i<21;i++) {
    strip2.setPixelColor(i, strip2.Color(red, green, blue));
  }

  strip2.show();

  strip2Red = red;
  strip2Green = green;
  strip2Blue = blue;
}

void strip2Third(int red, int green, int blue) {
  for (int i=21;i<32;i++) {
    strip2.setPixelColor(i, strip2.Color(red, green, blue));
  }

  strip2.show();

  strip2Red = red;
  strip2Green = green;
  strip2Blue = blue;
}

void strip2Fourth(int red, int green, int blue) {
  for (int i=32;i<42;i++) {
    strip2.setPixelColor(i, strip2.Color(red, green, blue));
  }

  strip2.show();

  strip2Red = red;
  strip2Green = green;
  strip2Blue = blue;
}

void strip2On(int red, int green, int blue) {
  
  for(int i=0;i<STRIP1_PIXELS;i++) {
      strip2.setPixelColor(i, strip2.Color(red, green, blue)); 
  } 

  strip2.show();
  
  strip2Red = red;
  strip2Green = green;
  strip2Blue = blue;
}

void matrixOn(int red, int green, int blue) {  

  for(int i=0;i<MATRIX_PIXELS;i++) {
      matrix.setPixelColor(i, matrix.Color(red, green, blue)); 
  } 
  
  matrix.show();
  
  matrixRed = red;
  matrixGreen = green;
  matrixBlue = blue;
}

void clearStrips() {
  clearstrip1();
  clearstrip2();
  clearmatrix();
}

void clearstrip1() {
  for(int i=0;i<STRIP1_PIXELS;i++){
    strip1.setPixelColor(i, strip1.Color(0, 0, 0));
  } 
  strip1.show();
}

void clearstrip2() {
  for(int i=0;i<STRIP2_PIXELS;i++){
    strip2.setPixelColor(i, strip2.Color(0, 0, 0));
  } 
  strip2.show();
}

void clearmatrix() {
  for(int i=0;i<MATRIX_PIXELS;i++){
    matrix.setPixelColor(i, matrix.Color(0, 0, 0));
  } 
  matrix.show();
}
